//paquetes
const dotenv = require('dotenv');
const express = require('express');
const firebase = require('firebase');
const faker = require('faker');
const app = express();
dotenv.config();

//configuracion de firebase con dotenv
const firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

//---------------------------------------------

//--funcion para llenar la base de datos
app.get('/populate/:numberOfEntries', (req, res) => {
  const { numberOfEntries } = req.params;
  const users = db.collection('users');

  for (let i = 0; i < numberOfEntries; i++) {
    let docRef = users.doc(faker.random.uuid());

    let setData = docRef.set({
      first: faker.name.firstName(),
      last: faker.name.lastName(),
      email: faker.internet.email(),
      address: faker.address.streetAddress(),
      city: faker.address.city(),
      phone: faker.phone.phoneNumber(),
      username: faker.internet.userName(),
      avatar: faker.image.avatar(),
    });
  }

  res.send(`Se crearon ${numberOfEntries} usuarios en la base de datos`);
});
//-------------------------------------------------------------------------


//--funcion get con async/await para generar un arreglo de datos alimentado
//por la base de datos y devolver un json con el tamaño y los usuarios
app.get('/users', async (req, res) => {
  let usersCollection = db.collection('users');
  const users = await usersCollection.get();
  const usersArray = [];

  users.forEach(doc => usersArray.push(doc.data()));

  res.json({
    length: usersArray.length,
    users: usersArray
  });
});
//------------------------------------------------------------------------------------

//3--tomamos el arreglo para buscar un id dentro de la informacion---------------------
app.get('/', async (req, res) => {
  const usersCollection = db.collection('users');
  const users = await usersCollection.get();
  // const first = req.query.first;
  // const last = req.query.last;
  let usersArray = [];

  users.forEach(doc => usersArray.push(doc.data()));
  const id = Math.floor(Math.random() * (usersArray.length - 1));

<<<<<<< HEAD
  if (req.query.hasOwnProperty('first')) {
    res.json({
      
      'first': usersArray[id]['first'],
    });
  }

  res.json(usersArray[id]);
});

=======
  const arrayOfKeys = Object.keys(req.query);

  let newObj = new Object();

  for (let element of arrayOfKeys) {
    newObj[element.toString()] = usersArray[id][element];
  }

  res.json(newObj);

});


>>>>>>> 2a36513f3a699d9010e664b7a816896ce149d435
app.listen(3000, () => console.log('Escuchando servidor en http://localhost:3000'));
