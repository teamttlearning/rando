// Programación funcional
const convertToArrayOfStrings = (arr) => arr.map((e) => typeof(e) === 'string' ? e : e.toString());

const convertToArrayOfStringsTwo = (arr) => {
  return arr.map(element => {
    return typeof(element) === 'string' ? element : element.toString();
  })
};

// Programación imperativa
const convertToArrayOfStringsWithFor = (arr) => {
  const newArr = [];

  for (let i = 0; i < arr.length; i++) {
    const newElement = typeof(arr[i]) === 'string' ? arr[i] : arr[i].toString();
    newArr.push(newElement);
  }

  return newArr;
};

const numbersLessOfThree = arr => arr.filter(e => e < 3);

const returnObjectWithFilter = (obj) => console.log(Object.keys(obj));

const objectA = {
  first: 'asdas',
  last: 'asdas',
};

returnObjectWithFilter(objectA);
